package main

import (
	"log"

	"github.com/elesq/proglog/internal/server"
)

func main() {

	port := ":8080"
	srv := server.NewHTTPServer(port)
	log.Println("Server listening on port: ", port)
	log.Fatal(srv.ListenAndServe())
}
