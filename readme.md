# Proglog projects

### Checkpoint 1
- Simple http service created with two end points to `post` and `get` from a log respectively.
- The Log is a structure holding a slice of Records and a mutex lock in memory.
- Can use simple curl requests to interact.
	- curl -X POST localhost:8080 -d '{"record": {"value": "TGV0J3MgR28gIzEK"}}'
	- curl -X GET localhost:8080 -d '{"offset": 0}'


### Checkpoint 2
- Introduces protobuf which allows us to encode our semantics once and use them across services to maintain a consistent data model.
	- guarantees type safety.
	- prevents schema violations.
	- enables fast serialization.
	- offers backward compatibility.
- Allows us to write and read our data from different data streams. Meaning we can communicate between 2 or more systems which is what we typically call a microservices architecture.

### Checkpoint 3
- Write a log package
	- Logs are critically important in distributed services. They following names are interchangeable:
		- _write-ahead logs_
		- _transaction logs_
		- _commit logs_
- To create a log we want to have:
	- our Log is segmented:
		- we cannot write to the same file forever, therefore we need segmentation.
		- Archived segments can be deleted for a good, useful approach to disk space management.
		- clean-up can run in background whilst keeping newest segment active and writeable.
		- we can have only one active segment.
		- a segment is made up of a store file (data) and an index file (index of store records).
	- a segment has sequential offset records of change.
		- fetch process reads index file to get position of a log record, then reads store to get detail of the record.
		- small index file (2 fields) means fast access, lookup, memory-map footprint.
- Updated terminology:
	- _record_ - the data stored in the log.
	- _store_ - the file we store records in.
	- _index_ - the file we store index entries in.
	- _segment_ - an abstraction that ties a store and an index together.
	- _log_ - an abstraction tying segments together.

### checkpoint 4
- Serve requests with gRPC:
	- without network capability the service requires a single person to learn the API, use the service on one computer and store the log on their own disk. This is not really feasible to expect. This section introduces network capability where we turn the service into a web service.
		- addresses availability and scalability.
		- allow multiple users to interact with the same data.
		- accessible interfaces which are easier to use and adopt.
- Add gRPC
	- simplicity:
		- because writing services is complex and hard we want to focus on problem it solves rather than req/res technical dissection.
		- works at mid/high levels. gRPC decides how to serialize and structure endpoints.
		- extensible via middleware.
	- maintainability:
		- first versions are a brief period in time, backwards compatibility is king.
		- req/res APIs typically run multiple instances.
		- gRPC allows easy write/run separate versions for breaking changes and using protobufs field versioning for small changes.
		- reqs/res being strongly typed prevents accidental breaking changes.
	- security:
		- supports SSL/TLS.
		- ease of use.
		- type checked:
			- methods, req, res, bodies all defined in types.
			- compiler copies comments from code as insight to usage.
			- can lookup service details with godoc.
	- performance:
		- being fast reduces resource usage and cost.
		- gRPC built on protobuf and http/2 ergo long lasting connections.
	- scalability:

		- scaling up with load balancing.
			- thick client-side load balancing.
			- proxy load balancing.
			- look-aside load balancing.
			- service mesh.
	- team organisation:
		- split into clients and servers.
		- accommodates language choices to be a consideration.
