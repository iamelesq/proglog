package log

import (
	"fmt"
	"os"
	"path"

	api "github.com/elesq/proglog/api/v1"
	"google.golang.org/protobuf/proto"
)

type segment struct {
	store      *store
	index      *index
	baseOffset uint64
	nextOffset uint64
	config     Config
}

// newSegment - called when a Log needs a new segment such as when the current active
// segment hits its max size. The function opens new store and index files with
// open/create flags (also append flag in the case of the storefile). It sets the
// nextOffset to prepare for the next appended record, where the index is empty that
// will be the 1st record, if one or more records exist it will be set to the number
// of existing records because of the baseOffset index zero rule.
func newSegment(dir string, baseOffset uint64, c Config) (*segment, error) {
	s := &segment{
		baseOffset: baseOffset,
		config:     c,
	}

	var err error

	storeFile, err := os.OpenFile(path.Join(dir, fmt.Sprintf("%d%s", baseOffset, ".store")), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return nil, err
	}

	if s.store, err = newStore(storeFile); err != nil {
		return nil, err
	}

	indexFile, err := os.OpenFile(path.Join(dir, fmt.Sprintf("%d%s", baseOffset, ".index")), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}

	if s.index, err = newIndex(indexFile, c); err != nil {
		return nil, err
	}

	if off, _, err := s.index.Read(-1); err != nil {
		s.nextOffset = baseOffset
	} else {
		s.nextOffset = baseOffset + uint64(off) + 1
	}

	return s, nil
}

// Append writes a record to the segment and returns the newly appended records offset.
// The log returns the offset to the API response. A segment appends a record in a
// two-step process. it adds the data to the store, then adds an index entry. Index
// offsets are relative to the base offset, subtract the next offset from the base
// offset to get the entry's relative offset in the segment. The next offset is
// incremented to prep for the next call.
func (s *segment) Append(record *api.Record) (offset uint64, err error) {
	cur := s.nextOffset
	record.Offset = cur
	p, err := proto.Marshal(record)
	if err != nil {
		return 0, err
	}

	_, pos, err := s.store.Append(p)
	if err != nil {
		return 0, err
	}

	// index offsets are relative to baseoffsets
	if err = s.index.Write(uint32(s.nextOffset-uint64(s.baseOffset)), pos); err != nil {
		return 0, err
	}

	s.nextOffset++
	return cur, nil
}

// Read returns the record for the given offset.
// To read a record the segment must translate the absolute index into a relative
// offset and get the associated index entry. Once this is known the segment can go
// straight to the records position in the store and read the proper amount of data.
func (s *segment) Read(off uint64) (*api.Record, error) {
	_, pos, err := s.index.Read(int64(off - s.baseOffset))
	if err != nil {
		return nil, err
	}

	p, err := s.store.Read(pos)
	if err != nil {
		return nil, err
	}

	record := &api.Record{}
	err = proto.Unmarshal(p, record)
	return record, err
}

// returns whether a segment has reached its max size. The size can be
// reached in two ways:
// writing a small number of long logs and hitting the bytes limit,
// or writing a larger number of small logs, hitting the index bytes
// limits.
func (s *segment) IsMaxed() bool {
	return s.store.size >= s.config.Segment.MaxStoreBytes || s.index.size >= s.config.Segment.MaxIndexBytes
}

func (s *segment) Remove() error {
	if err := s.Close(); err != nil {
		return err
	}

	if err := os.Remove(s.index.Name()); err != nil {
		return err
	}

	if err := os.Remove(s.store.Name()); err != nil {
		return err
	}

	return nil
}

// Close checks for errors closing the index and store in turn.
func (s *segment) Close() error {
	if err := s.index.Close(); err != nil {
		return err
	}

	if err := s.store.Close(); err != nil {
		return err
	}

	return nil
}

// returns the nearest and lesser multiple of k in j.
// eg nearestMultiple(9, 4) == 8
// Always takes the lesser multiple to ensure it stays
// under the users disk capacity.
func nearestMultiple(j, k uint64) uint64 {
	if j == 0 {
		return (j / k) * k
	}
	return ((j - k + 1) / k) * k
}
