package server

import (
	"fmt"
	"sync"
)

type Record struct {
	Value  []byte `json:"value"`
	Offset uint64 `json:"offset"`
}

type Log struct {
	mu      sync.Mutex
	records []Record
}

var ErrorOffsetNotFound = fmt.Errorf("offset not found")

func NewLog() *Log {
	return &Log{}
}

// Append accepts a record type and will return the offset position
// the record was entered into the log.records at. The structure is
// locked using a mutex lock and unlocked on completion. If any error is
// encountered the error detail will be returned instread of a position
// in the log.
func (c *Log) Append(record Record) (uint64, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	record.Offset = uint64(len(c.records))
	c.records = append(c.records, record)
	return record.Offset, nil
}

// Read will return the record found at the position of the records
// struct matching the offset passed. If no record is found or the offset
// passed exceeds the length of the records structure then a custom
// error is thrown to declare the offset was not found.
func (c *Log) Read(offset uint64) (Record, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if offset >= uint64(len(c.records)) {
		return Record{}, ErrorOffsetNotFound
	}

	return c.records[offset], nil
}
